# DeltaVR - Multiplayer Builds



## Folders

**Delta_vr** - This holds the PCVR version of DeltaVR - multiplayer. Running DeltaVR.exe will open the application in SteamVR.

**Delta_novr** - This holds the PC non-VR version of DeltaVR - multiplayer. Running DeltaVR.exe will open the application.

**delta_vr_android** - This holds the .apk file of the Quest 2 version of DeltaVR - multiplayer. This .apk file will have to be sideloaded to your Quest 2 device and started from the unknown apps section of the application menu.
